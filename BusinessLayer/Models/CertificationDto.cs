﻿namespace BusinessLayer.Models
{
    public class CertificationDto
    {
        public int Id { get; set; }

        public int Name { get; set; }

        public int SortOrder { get; set; }
    }
}
