﻿namespace BusinessLayer.Models
{
    public class AssociationValueDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string Type { get; set; }
    }
}
