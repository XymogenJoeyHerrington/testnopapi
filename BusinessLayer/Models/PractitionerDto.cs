﻿using System.Collections.Generic;

namespace BusinessLayer.Models
{
    public class PractitionerDto
    {
		public int Id { get; set; }

		public string SapID { get; set; }

		public string ReferralCode { get; set; }

		public string LastName { get; set; }

		public int StoreId { get; set; }

		public string Website { get; set; }

		public string Notes { get; set; }

		public int AccessLevel { get; set; }

		public string Company { get; set; }

		public string CompanyDescription { get; set; }

		public string DispensaryLinkUrlPart { get; set; }

		public bool IsDefault { get; set; }

		public bool IsConfigured { get; set; }

		public int Discount { get; set; }

		public IEnumerable<PractitionerAddressDto> Addresses { get; set; }

		public IEnumerable<CertificationDto> Certifications { get; set; }

		public IEnumerable<AssociationValueDto> Languages { get; set; }

		public IEnumerable<AssociationValueDto> Specialities { get; set; }

		public IEnumerable<AssociationValueDto> Genders { get; set; }
	}
}
