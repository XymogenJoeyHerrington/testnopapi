﻿using System;

namespace BusinessLayer.Models
{
    public class HoursOfOperationsDto
    {
        public int Id { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }
    }
}
