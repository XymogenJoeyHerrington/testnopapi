﻿using System.Linq;
using BusinessLayer.Models;
using DatabaseService;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer
{
    public class DispensaryRepository : IDispensaryRepository
    {
        private readonly NopApiDbContext _dbContext;

        public DispensaryRepository(NopApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<PractitionerDto> GetPractitioner(int storeId, string sapId) => _dbContext.PractitionerProfiles
            .Include(p => p.Addresses).ThenInclude(a => a.Values).ThenInclude(v => v.Type)
            .Include(p => p.Addresses).ThenInclude(a => a.HoursOfOperations)
            .Include(p => p.Values).ThenInclude(v => v.Type)
            .Include(p => p.Certifications)
            .Where(p => !p.IsDeleted && p.StoreId == storeId && p.SapID == sapId)
            .Select(p => new PractitionerDto
            {
                Id = p.Id,
                SapID = p.SapID,
                ReferralCode = p.ReferralCode,
                LastName = p.LastName,
                StoreId = p.StoreId,
                Website = p.Website,
                Notes = p.Notes,
                AccessLevel = p.AccessLevel,
                Company = p.Company,
                CompanyDescription = p.CompanyDescription,
                DispensaryLinkUrlPart = p.DispensaryLinkUrlPart,
                IsDefault = p.IsDefault,
                IsConfigured = p.IsConfigured,
                Discount = p.Discount,
                Addresses = p.Addresses
                    .Select(a => new PractitionerAddressDto
                    {
                        Id = a.Id,
                        Address = a.Address,
                        Address2 = a.Address2,
                        City = a.City,
                        State = a.State,
                        Country = a.Country,
                        Zip = a.Zip,
                        TimeZone = a.TimeZone,
                        Phone = a.Phone,
                        Email = a.Email,
                        HoursOfOperations = a.HoursOfOperations
                            .Select(hoa => new HoursOfOperationsDto
                            {
                                Id = hoa.Id,
                                DayOfWeek = hoa.DayOfWeek,
                                StartTime = hoa.StartTime,
                                EndTime = hoa.EndTime
                            }),
                        AdditionalInformation = a.Values
                            .Where(v => v.Type.TypeName == "AdditionalInformation")
                            .Select(v => new AssociationValueDto
                            {
                                Id = v.Id,
                                Name = v.DisplayName,
                                Value = v.ValueName,
                                Type = v.Type.TypeName
                            })
                    }),
                Certifications = p.Certifications
                    .Select(c=>new CertificationDto
                    {
                        Id = c.Id,
                        Name = c.CertificateName,
                        SortOrder = c.SortOrder
                    }),
                Languages = p.Values
                    .Where(v => v.Type.TypeName == "Language")
                    .Select(v => new AssociationValueDto
                    {
                        Id = v.Id,
                        Name = v.DisplayName,
                        Value = v.ValueName,
                        Type = v.Type.TypeName
                    }),
                Specialities = p.Values
                    .Where(v => v.Type.TypeName == "Specialty")
                    .Select(v => new AssociationValueDto
                    {
                        Id = v.Id,
                        Name = v.DisplayName,
                        Value = v.ValueName,
                        Type = v.Type.TypeName
                    }),
                Genders = p.Values
                    .Where(v => v.Type.TypeName == "Gender")
                    .Select(v => new AssociationValueDto
                    {
                        Id = v.Id,
                        Name = v.DisplayName,
                        Value = v.ValueName,
                        Type = v.Type.TypeName
                    })
            });
    }
}
