﻿using System.Linq;
using BusinessLayer.Models;

namespace BusinessLayer
{
    public interface IDispensaryRepository
    {
        IQueryable<PractitionerDto> GetPractitioner(int storeId, string sapId);
    }
}
