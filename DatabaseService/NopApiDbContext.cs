﻿using System.Diagnostics.CodeAnalysis;
using DataModels;
using Microsoft.EntityFrameworkCore;

namespace DatabaseService
{
    public class NopApiDbContext : DbContext
    {
        public NopApiDbContext([NotNull] DbContextOptions<NopApiDbContext> options)
            : base(options)
        { }

        public DbSet<PractitionerProfile> PractitionerProfiles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfigurationsFromAssembly(typeof(NopApiDbContext).Assembly);
        }
    }
}
