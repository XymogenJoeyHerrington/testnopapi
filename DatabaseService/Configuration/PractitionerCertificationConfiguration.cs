﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class PractitionerCertificationConfiguration : IEntityTypeConfiguration<PractitionerCertification>
    {
        public void Configure(EntityTypeBuilder<PractitionerCertification> builder)
        {
            builder.ToTable("xy_PractitionerProfile_Certifications", "dbo");
        }
    }
}
