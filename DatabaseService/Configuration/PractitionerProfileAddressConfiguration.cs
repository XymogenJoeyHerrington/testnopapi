﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class PractitionerProfileAddressConfiguration : IEntityTypeConfiguration<PractitionerProfileAddress>
    {
        public void Configure(EntityTypeBuilder<PractitionerProfileAddress> builder)
        {
            builder.ToTable("xy_STORE_PractitionerProfile_Addresses", "dbo");

            builder
                .HasMany(t => t.Values)
                .WithMany(t => t.Addresses)
                .UsingEntity<PractitionerProfileAddressAssociationValue>(
                    configureRight: t => t
                        .HasOne(c => c.Value)
                        .WithMany(c => c.AddressAssociationValues)
                        .HasForeignKey(c => c.AssociationValueId),
                    configureLeft: t => t
                        .HasOne(c => c.Address)
                        .WithMany(c => c.AssociationValues)
                        .HasForeignKey(c => c.PractitionerProfileAddressId),
                    configureJoinEntityType: t => {
                        t.ToTable("xy_PractitionerProfileAddress_AssociationValues", "dbo");
                        t.HasKey(c => new { c.PractitionerProfileAddressId, c.AssociationValueId });
                    });

            builder
                .HasMany(t => t.HoursOfOperations)
                .WithOne(t => t.Address)
                .HasForeignKey(t => t.PractitionerProfileAddressId);
        }
    }
}
