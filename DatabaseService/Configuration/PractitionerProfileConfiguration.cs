﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class PractitionerProfileConfiguration : IEntityTypeConfiguration<PractitionerProfile>
    {
        public void Configure(EntityTypeBuilder<PractitionerProfile> builder)
        {
            builder.ToTable("xy_STORE_PractitionerProfile", "dbo");

            builder
                .HasMany(t => t.Addresses)
                .WithOne(t => t.PractitionerProfile)
                .HasForeignKey(t => t.PractitionerProfileId);

            builder
                .HasMany(t => t.Values)
                .WithMany(t => t.Practitioners)
                .UsingEntity<PractitionerProfileAssociationValue>(
                    configureRight: t => t
                        .HasOne(c => c.Value)
                        .WithMany(c => c.PractitionerAssociationValues)
                        .HasForeignKey(c => c.AssociationValueId),
                    configureLeft: t => t
                        .HasOne(c => c.Practitioner)
                        .WithMany(c => c.AssociationValues)
                        .HasForeignKey(c => c.PractitionerProfileId),
                    configureJoinEntityType: t => {
                        t.ToTable("xy_PractitionerProfile_AssociationValues", "dbo");
                        t.HasKey(c => new { c.PractitionerProfileId, c.AssociationValueId });
                    });

            builder
                .HasMany(t => t.Certifications)
                .WithOne(t => t.Practitioner)
                .HasForeignKey(t => t.PractitionerProfileId);
        }
    }
}
