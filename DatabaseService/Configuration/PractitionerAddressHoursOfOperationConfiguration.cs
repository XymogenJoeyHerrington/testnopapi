﻿using System;
using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class PractitionerAddressHoursOfOperationConfiguration : IEntityTypeConfiguration<PractitionerAddressHoursOfOperation>
    {
        public void Configure(EntityTypeBuilder<PractitionerAddressHoursOfOperation> builder)
        {
            builder.ToTable("xy_PractitionerProfileAddress_HoursOfOperation", "dbo");

            builder
                .Property(t => t.DayOfWeek)
                .HasConversion(t => (int)t, t => (DayOfWeek)t);
        }
    }
}
