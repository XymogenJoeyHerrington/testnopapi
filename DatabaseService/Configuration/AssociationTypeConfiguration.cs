﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class AssociationTypeConfiguration : IEntityTypeConfiguration<AssociationType>
    {
        public void Configure(EntityTypeBuilder<AssociationType> builder)
        {
            builder.ToTable("xy_AssociationTypes", "dbo");
        }
    }
}
