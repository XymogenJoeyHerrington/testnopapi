﻿using DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DatabaseService.Configuration
{
    public class AssociationValueConfiguration : IEntityTypeConfiguration<AssociationValue>
    {
        public void Configure(EntityTypeBuilder<AssociationValue> builder)
        {
            builder.ToTable("xy_AssociationValues", "dbo");

            builder
                .HasOne(t => t.Type)
                .WithMany(t => t.Values)
                .HasForeignKey(t => t.TypeId);
        }
    }
}
