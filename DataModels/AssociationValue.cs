﻿using System;
using System.Collections.Generic;

namespace DataModels
{
    public class AssociationValue
    {
        public int Id { get; set; }

        public int TypeId { get; set; }

        public string ValueName { get; set; }

        public string DisplayName { get; set; }

        public int? SortOrder { get; set; }

        public DateTime CreateDate { get; set; }

        public short? CreatedBy { get; set; }

        public DateTime? UdpateDate { get; set; }

        public short? UdpatedBy { get; set; }

        public virtual AssociationType Type { get; protected set; }

        public virtual ICollection<PractitionerProfile> Practitioners { get; set; }

        public virtual ICollection<PractitionerProfileAssociationValue> PractitionerAssociationValues { get; set; }

        public virtual ICollection<PractitionerProfileAddress> Addresses { get; set; }

        public virtual ICollection<PractitionerProfileAddressAssociationValue> AddressAssociationValues { get; set; }
    }
}
