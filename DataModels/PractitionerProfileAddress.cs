﻿using System;
using System.Collections.Generic;

namespace DataModels
{
    public class PractitionerProfileAddress
    {
        public int Id { get; set; }

        public int PractitionerProfileId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string TimeZone { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public virtual PractitionerProfile PractitionerProfile { get; protected set; }

        public virtual ICollection<PractitionerAddressHoursOfOperation> HoursOfOperations { get; protected set; }

        public virtual ICollection<AssociationValue> Values { get; set; }

        public virtual ICollection<PractitionerProfileAddressAssociationValue> AssociationValues { get; set; }
    }
}
