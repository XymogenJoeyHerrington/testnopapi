﻿using System;
using System.Collections.Generic;

namespace DataModels
{
    public class PractitionerProfile
    {
		public int Id { get; set; }

		public string SapID { get; set; }

		public string ReferralCode { get; set; }

		public string LastName { get; set; }

		public int StoreId { get; set; }

		public string Website { get; set; }

		public string Notes { get; set; }

		public int AccessLevel { get; set; }

		public string Company { get; set; }

		public byte[] CompanyLogoImageData { get; set; }

		public string CompanyLogoImageMimeType { get; set; }

		public int? CompanyLogoImageLength { get; set; }

		public string CompanyDescription { get; set; }

		public string DispensaryLinkUrlPart { get; set; }

		public bool IsDefault { get; set; }

		public bool IsDeleted { get; set; }

		public bool IsConfigured { get; set; }

		public int Discount { get; set; }

		public DateTime UpdatedDate { get; set; }

		public DateTime CreatedDate { get; set; }

		public virtual ICollection<PractitionerProfileAddress> Addresses { get; protected set; }

		public virtual ICollection<PractitionerCertification> Certifications { get; protected set; }

		public virtual ICollection<AssociationValue> Values { get; set; }

		public virtual ICollection<PractitionerProfileAssociationValue> AssociationValues { get; set; }
	}
}
