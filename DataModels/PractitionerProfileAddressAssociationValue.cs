﻿using System;

namespace DataModels
{
    public class PractitionerProfileAddressAssociationValue
    {
        public int PractitionerProfileAddressId { get; set; }

        public int AssociationValueId { get; set; }

        public DateTime CreateDate { get; set; }

        public short? CreatedBy { get; set; }

        public DateTime? UdpateDate { get; set; }

        public short? UdpatedBy { get; set; }

        public virtual AssociationValue Value { get; set; }

        public virtual PractitionerProfileAddress Address { get; set; }
    }
}
