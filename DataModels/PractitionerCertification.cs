﻿using System;

namespace DataModels
{
    public class PractitionerCertification
    {
        public int Id { get; set; }

		public int PractitionerProfileId { get; set; }

		public int CertificateName { get; set; }

		public int SortOrder { get; set; }

		public DateTime CreateDate { get; set; }

		public short? CreatedBy { get; set; }

		public DateTime? UdpateDate { get; set; }

		public short? UdpatedBy { get; set; }

		public virtual PractitionerProfile Practitioner  { get; set; }
    }
}
