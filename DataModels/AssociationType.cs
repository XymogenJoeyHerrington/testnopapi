﻿using System.Collections.Generic;

namespace DataModels
{
    public class AssociationType
    {
        public int Id { get; set; }

        public string TypeName { get; set; }

        public string DisplayName { get; set; }

        public virtual ICollection<AssociationValue> Values { get; set; }
    }
}
