﻿using System;

namespace DataModels
{
    public class PractitionerAddressHoursOfOperation
    {
        public int Id { get; set; }

		public int PractitionerProfileAddressId { get; set; }

		public DayOfWeek DayOfWeek { get; set; }

		public TimeSpan StartTime { get; set; }

		public TimeSpan EndTime { get; set; }

		public DateTime CreateDate { get; set; }

		public short? CreatedBy { get; set; }

		public DateTime? UdpateDate { get; set; }

		public short? UdpatedBy { get; set; }

		public virtual PractitionerProfileAddress Address { get; protected set; }
    }
}
