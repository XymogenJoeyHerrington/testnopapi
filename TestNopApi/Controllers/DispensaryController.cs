﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestNopApi.Models;

namespace TestNopApi.Controllers
{
    [ApiController]
    [Route("store/{storeId}/practitioner/{practitionerId}")]
    public class DispensaryController : ControllerBase
    {
        private readonly ILogger<DispensaryController> _logger;

        public DispensaryController(
            ILogger<DispensaryController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<PractitionerDto> GetPractitioner(int storeId, string practitionerId) => new PractitionerDto
        {
            Id = 12345,
            Name = "Dr. John Smith",
            ClinicName = "Advanced Nutrition Center",
            Description = "Blah blah blah",
            Website = "https://www.advancednutrition.com",
            Image = new PractitionerImageDto
            {
                Data = Encoding.Default.GetBytes("ABC123"),
                MineType = "image/png",
                Length = 43468
            },
            ReferralCodes = new List<PractitionerReferralDto>
            {
                new PractitionerReferralDto
                {
                    Id = 123,
                    Code = "DRMARIO21",
                    LastName = "MARCUS",
                    DispensaryLink = "drmario21",
                    Discount = 5
                }
            },
            Addresses = new List<PractitionerAddressDto>
            {
                new PractitionerAddressDto
                {
                    Id = 123,
                    Address = "5755 North Point Parkway",
                    Address2 = "Suite 217",
                    City = "Orlando",
                    State = "Fl",
                    Country = "US", // do we actually need this?
                    Postal = "32819-12345",
                    Phone = "(407) 123-4567",
                    Email = "emaildrmario@drmario.com",
                    TimeZone = "est",
                    HoursOfOperations = new List<HoursOfOperationsDto>
                    {
                        new HoursOfOperationsDto
                        {
                            DayOfWeek = DayOfWeek.Monday,
                            From = new TimeSpan(9, 0, 0),
                            To = new TimeSpan(16, 0, 0)
                        },
                        new HoursOfOperationsDto
                        {
                            DayOfWeek = DayOfWeek.Tuesday,
                            From = new TimeSpan(9, 0, 0),
                            To = new TimeSpan(16, 0, 0)
                        },
                        new HoursOfOperationsDto
                        {
                            DayOfWeek = DayOfWeek.Wednesday,
                            From = new TimeSpan(9, 0, 0),
                            To = new TimeSpan(14, 0, 0)
                        },
                        new HoursOfOperationsDto
                        {
                            DayOfWeek = DayOfWeek.Saturday,
                            From = new TimeSpan(11, 0, 0),
                            To = new TimeSpan(14, 0, 0)
                        },
                    },
                    AdditionalInformation = new List<string> { "InsuranceAccepted", "VirtualConsultations", "OnlineBooking" }
                }
            },
            Certifications = new List<string> { "IFM Certified Doctor of Medicine", "Certified Doctor of Awesomeness" },
            Gender = "Female",
            Specialty = "functional-medicine",
            Languages = new List<string> { "english", "spanish" }
        };

        [HttpPut]
        public IActionResult PutPractitioner(int storeId, string practitionerId, [FromBody] PractitionerDto practitioner) => NoContent();

        [HttpPut("details")]
        public IActionResult PutPractitionerDetails(int storeId, string practitionerId, [FromBody] PractitionerDetailsDto details) => NoContent();

        [HttpGet("referral/{referralId}")]
        public ActionResult<PractitionerReferralDto> GetPractitionerReferral(int storeId, string practitionerId, int referralId) => new PractitionerReferralDto
        {
            Id = 123,
            Code = "DRMARIO21",
            LastName = "MARCUS",
            DispensaryLink = "drmario21",
            Discount = 5
        };

        [HttpPost("referral")]
        public IActionResult PostPractitionerReferral(int storeId, string practitionerId, [FromBody] PractitionerReferralDto referral)
        {
            referral.Id = 12345;
            return CreatedAtAction(
                nameof(GetPractitionerReferral),
                new { storeId, practitionerId, referralId = 12345 },
                referral);
        }

        [HttpPut("referral/{referralId}")]
        public IActionResult PutPractitionerReferral(int storeId, string practitionerId, int referralId, [FromBody] PractitionerReferralDto referral) => NoContent();

        [HttpPut("referral/{referralId}/link")]
        public IActionResult PutPractitionerReferralDispensaryLink(int storeId, string practitionerId, int referralId, [FromBody] PractitionerReferralDto referral)
        {
            // only consider the DispensaryLink property, ignore the others.
            return NoContent();
        }

        [HttpGet("address/{addressId}")]
        public ActionResult<PractitionerAddressDto> GetPractitionerAddress(int storeId, string practitionerId, int addressId) => new PractitionerAddressDto
        {
            Id = addressId,
            Address = "5755 North Point Parkway",
            Address2 = "Suite 217",
            City = "Orlando",
            State = "Fl",
            Country = "US", // do we actually need this?
            Postal = "32819-12345",
            Phone = "(407) 123-4567",
            Email = "emaildrmario@drmario.com",
            TimeZone = "est",
            HoursOfOperations = new List<HoursOfOperationsDto>
            {
                new HoursOfOperationsDto
                {
                    DayOfWeek = DayOfWeek.Monday,
                    From = new TimeSpan(9, 0, 0),
                    To = new TimeSpan(16, 0, 0)
                },
                new HoursOfOperationsDto
                {
                    DayOfWeek = DayOfWeek.Tuesday,
                    From = new TimeSpan(9, 0, 0),
                    To = new TimeSpan(16, 0, 0)
                },
                new HoursOfOperationsDto
                {
                    DayOfWeek = DayOfWeek.Wednesday,
                    From = new TimeSpan(9, 0, 0),
                    To = new TimeSpan(14, 0, 0)
                },
                new HoursOfOperationsDto
                {
                    DayOfWeek = DayOfWeek.Saturday,
                    From = new TimeSpan(11, 0, 0),
                    To = new TimeSpan(14, 0, 0)
                },
            },
            AdditionalInformation = new List<string> { "InsuranceAccepted", "VirtualConsultations", "OnlineBooking" }
        };

        [HttpPost("address")]
        public IActionResult PostPractitionerAddress(int storeId, string practitionerId, [FromBody] PractitionerAddressDto address)
        {
            address.Id = 12345;
            return CreatedAtAction(
                nameof(GetPractitionerAddress),
                new { storeId, practitionerId, addressId = 12345 },
                address);
        }

        [HttpPut("address/{addressId}")]
        public IActionResult PutPractitionerAddress(int storeId, string practitionerId, int addressId, [FromBody] PractitionerAddressDto address) => NoContent();

        [HttpDelete("address/{addressId}")]
        public IActionResult DeletePractitionerAddress(int storeId, string practitionerId, int addressId) => NoContent();
    }
}

