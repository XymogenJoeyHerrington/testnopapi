﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TestNopApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LinksController : ControllerBase
    {
        private readonly ILogger<LinksController> _logger;

        public LinksController(
            ILogger<LinksController> logger)
        {
            _logger = logger;
        }


    }
}
