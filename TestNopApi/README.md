﻿# Links / Dispensary API

Here's a summary of API endpoints and necessary database changes to support
those endpoints.

## Api Endpoints

The following are a list of `GET`, `POST`, `PUT` and `DELETE` API endpoints
needed to support the Dispensary / Links features.


### Fetch Dispensary Information

Fetches a practitioner's dispensary information for display on the WholeScripts
Practitioner Dispensary page.  

#### Route:
`GET` store/`[storeId]`/practitioner/`[practitionerId]`

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner

#### Response Values:
* __`name`__
* __`clinic-name`__
* __`description`__
* __`website`__
* __`image`__: complex structure used to construct an image
  * __`data`__: deserialize into a byte array
  * __`mime-type`__
  * __`length`__: length of the data in bytes
* __`referral-codes`__: list of referral code data
  * __`id`__
  * __`code`__
  * __`last-name`__
  * __`dispensary-link`__: referral key to append at the back of the dispensary link
  * __`discount`__: discount percentage
* __`addresses`__: list of locations associated with this business
  * __`id`__
  * __`address`__
  * __`address2`__
  * __`city`__
  * __`state`__
  * __`postal`__
  * __`email`__
  * __`time-zone`__: `"est"`, `"cst"`, `"mst"`, or `"pst"`
  * __`hours-of-operation`__: list containing hours of operation data
    * __`day-of-week`__: `"Sunday"` through `"Saturday"`
    * __`from`__: deserialize to a timespan
    * __`to`__: deserialize to a timespan
  * __`additional-information`__: List containing the `value` from the additional information items

#### Example Response Body:
``` json
{
  "name": "Dr. John Smith",
  "clinic-name": "Advanced Nutrition Center",
  "description": "Blah blah blah",
  "website": "https://www.advancednutrition.com",
  "image": {
    "data": "ABC123",
    "mime-type": "image/png",
    "length": 43468
  },
  "referral-codes": [
    {
      "id": 123,
      "code": "DRMARIO21",
      "last-name": "MARCUS",
      "dispensary-link": "drmario21",
      "discount": 5
    }
  ],
  "addresses": [
    {
      "id": 123,
      "address": "5755 North Point Parkway",
      "address2": "Suite 217",
      "city": "Orlando",
      "state": "FL",
      "postal": "32819-12345",
      "phone": "(407) 123-5467",
      "email": "emaildrmario@drmario.com",
      "time-zone": "est",
      "hours-of-operations": [
        {
          "day-of-week": "Monday",
          "from": "09:00:00",
          "to": "16:00:00",
        },
        {
          "day-of-week": "Tuesday",
          "from": "09:00:00",
          "to": "16:00:00",
        },
        {
          "day-of-week": "Wednesday",
          "from": "09:00:00",
          "to": "14:00:00",
        },
        {
          "day-of-week": "Saturday",
          "from": "11:00:00",
          "to": "14:00:00",
        },
      ],
      "additional-information": [
        "InsuranceAccepted",
        "VirtualConsultations",
        "OnlineBooking"
      ]
    }
  ]
}
```

_______________________________________________________________________________


### 





### Save Basic Info
Saves the basic info for the practitioner

#### Route:
`PUT` store/`[storeId]`/practitioner/`[practitionerId]`

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner

#### Request Body
* __`clinic-name`__
* __`description`__
* __`website`__
* __`image`__: complex structure used to construct an image.  Due to the size,
this is optional and should only be included if it's changed!
  * __`data`__: deserialize into a byte array
  * __`mime-type`__
  * __`length`__: length of the data in bytes

#### Example Request Body:
```json
{
  "clinic-name": "Advanced Nutrition Center",
  "description": "Blah blah blah",
  "website": "https://www.advancednutrition.com",
  "image": {
    "data": "ABC123",
    "mime-type": "image/png",
    "length": 43468
  }
}
```

_______________________________________________________________________________

### Save Additional Details
Saves the additional details for the dispensary for the practitioner

#### Route:
`PUT` store/`[storeId]`/practitioner/`[practitionerId]`/details

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner

#### Request Body
* __`certifications[]`__: List of strings
* __`gender`__: Male or Female
* __`specialty`__: string - the `value` field from the specialties dropdown
* __`languages`__:  List of strings

#### Example Request Body
```json
{
  "certifications": [
    "IFM Certified Doctor of Medicine",
    "Doctor of awesomeness"
  ],
  "gender": "male",
  "specialty": "FamilyMedicine",
  "languages": [
    "Arabic",
    "English",
    "French"
  ]
}
```

_______________________________________________________________________________
### Create Referral
Adds a new referral record to the database

#### Route:
`POST` store/`[storeId]`/practitioner/`[practitionerId]`/referral

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner

#### Request Body
* __`referral-code`__: string unique referral code
* __`last-name`__: Practitioner last name
* __`discount`__: numeric value

#### Example Request Body
```json
{
  "referral-code": "DRMARIO21",
  "last-name": "MARCUS",
  "discount": 20,
}
```
_______________________________________________________________________________

### Save Dispensary Link

Saves the referral link for the practitioner's referral as specified by the
referral Id

#### Route:
`PUT` store/`[storeId]`/practitioner/`[practitionerId]`/referral/[referralId]

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner
* __`referralId`__: (integer) Referral ID

#### Request Body
* __`dispensary-link`__

#### Example Request Body:
```json
{
  "dispensary-link": "drmario21",
}
```

_______________________________________________________________________________

### Save Dispensary Settings
Saves the dispensary settings for the practitioner's referral as specified by the
referral Id

#### Route:
`PUT` store/`[storeId]`/practitioner/`[practitionerId]`/referral/[referralId]

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner
* __`referralId`__: (integer) Referral ID

#### Request Body
* __`dispensary-link`__

#### Example Request Body:
```json
{
  "dispensary-link": "drmario21",
}
```

_______________________________________________________________________________

### Save Address
Saves the address for the practitioner as specified by the
address Id

#### Route:
`PUT` store/`[storeId]`/practitioner/`[practitionerId]`/address/[addressId]

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner
* __`addressId`__: (integer) Address ID

#### Request Body
* __`address`__
* __`address2`__
* __`city`__
* __`state`__
* __`postal`__
* __`email`__
* __`time-zone`__: `"est"`, `"cst"`, `"mst"`, or `"pst"`
* __`hours-of-operation`__: list containing hours of operation data
  * __`day-of-week`__: `"Sunday"` through `"Saturday"`
  * __`from`__: deserialize to a timespan
  * __`to`__: deserialize to a timespan
* __`additional-information`__: List `value` from the additional data dropdown

#### Example Request Body:
```json
{
  "address": "5755 North Point Parkway",
  "address2": "Suite 217",
  "city": "Orlando",
  "state": "FL",
  "postal": "32819-12345",
  "phone": "(407) 123-5467",
  "email": "emaildrmario@drmario.com",
  "time-zone": "est",
  "hours-of-operations": [
      {
        "day-of-week": "Monday",
        "from": "09:00:00",
        "to": "16:00:00",
      },
      {
        "day-of-week": "Tuesday",
        "from": "09:00:00",
        "to": "16:00:00",
      },
      {
        "day-of-week": "Wednesday",
        "from": "09:00:00",
        "to": "14:00:00",
      },
      {
        "day-of-week": "Saturday",
        "from": "11:00:00",
        "to": "14:00:00",
      },
  ],
  "additional-information": [
    "InsuranceAccepted",
    "VirtualConsultations",
    "OnlineBooking"
  ]
}
```

_______________________________________________________________________________

### Add Address
Adds a new address for the practitioner returning a new address Id

#### Route:
`POST` store/`[storeId]`/practitioner/`[practitionerId]`/address/

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner

#### Request Body
* __`address`__
* __`address2`__
* __`city`__
* __`state`__
* __`postal`__
* __`email`__
* __`time-zone`__: `"est"`, `"cst"`, `"mst"`, or `"pst"`
* __`hours-of-operation`__: list containing hours of operation data
  * __`day-of-week`__: `"Sunday"` through `"Saturday"`
  * __`from`__: deserialize to a timespan
  * __`to`__: deserialize to a timespan
* __`additional-information`__: List containing name/value pairs of selected additional data
  * __`name`__
  * __`value`__

#### Example Request Body:
```json
{
  "address": "5755 North Point Parkway",
  "address2": "Suite 217",
  "city": "Orlando",
  "state": "FL",
  "postal": "32819-12345",
  "phone": "(407) 123-5467",
  "email": "emaildrmario@drmario.com",
  "time-zone": "est",
  "hours-of-operations": [
      {
        "day-of-week": "Monday",
        "from": "09:00:00",
        "to": "16:00:00",
      },
      {
        "day-of-week": "Tuesday",
        "from": "09:00:00",
        "to": "16:00:00",
      },
      {
        "day-of-week": "Wednesday",
        "from": "09:00:00",
        "to": "14:00:00",
      },
      {
        "day-of-week": "Saturday",
        "from": "11:00:00",
        "to": "14:00:00",
      },
  ],
  "additional-information": [
    {
      "name": "Insurance Accepted",
      "value": "InsuranceAccepted"
    },
    {
      "name": "Virtual Consultations",
      "value": "VirtualConsultations"
    },
    {
      "name": "Online Booking",
      "value": "OnlineBooking"
    },
  ]
}
```

#### Response Values
* __`id`__

#### Example Response Body:
```json
{
  "id": 12345
}
```

_______________________________________________________________________________

### Delete Address
Removes an address from the practitioner provided by an address Id

#### Route:
`DELETE` store/`[storeId]`/practitioner/`[practitionerId]`/address/[addressId]

#### Route Values:
* __`storeId`__: (integer) Should be 1
* __`practitionerId`__: (string) SAP Id of the practitioner
* __`addressId`__: (integer) Address ID









