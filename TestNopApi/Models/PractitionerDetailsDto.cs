﻿using System.Collections.Generic;

namespace TestNopApi.Models
{
    public class PractitionerDetailsDto
    {
        public List<string> Certifications { get; set; }

        public string Gender { get; set; }

        public string Specialty { get; set; }

        public List<string> Languages { get; set; }
    }
}
