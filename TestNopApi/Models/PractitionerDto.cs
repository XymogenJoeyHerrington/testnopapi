﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TestNopApi.Models
{
    public class PractitionerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [JsonPropertyName("clinic-name")]
        public string ClinicName { get; set; }

        public string Description { get; set; }

        public string Website { get; set; }

        public PractitionerImageDto Image { get; set; }

        [JsonPropertyName("referral-codes")]
        public List<PractitionerReferralDto> ReferralCodes { get; set; }

        public List<PractitionerAddressDto> Addresses { get; set; }

        public List<string> Certifications { get; set; }

        public string Gender { get; set; }

        public string Specialty { get; set; }

        public List<string> Languages { get; set; }
    }
}
