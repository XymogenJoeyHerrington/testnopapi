﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TestNopApi.Models
{
    public class PractitionerAddressDto
    {
        public int Id { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string Postal { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        [JsonPropertyName("time-zone")]
        public string TimeZone { get; set; }

        [JsonPropertyName("hours-of-operation")]
        public List<HoursOfOperationsDto> HoursOfOperations { get; set; }

        [JsonPropertyName("additional-information")]
        public List<string> AdditionalInformation { get; set; }
    }
}
