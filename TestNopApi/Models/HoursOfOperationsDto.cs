﻿using System;
using System.Text.Json.Serialization;

namespace TestNopApi.Models
{
    public class HoursOfOperationsDto
    {
        [JsonPropertyName("day-of-week")]
        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan From { get; set; }

        public TimeSpan To { get; set; }
    }
}
