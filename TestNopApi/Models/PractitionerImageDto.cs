﻿using System.Text.Json.Serialization;

namespace TestNopApi.Models
{
    public class PractitionerImageDto
    {
        public byte[] Data { get; set; }

        [JsonPropertyName("mime-type")]
        public string MineType { get; set; }

        public int Length { get; set; }
    }
}
