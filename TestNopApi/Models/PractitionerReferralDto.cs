﻿using System.Text.Json.Serialization;

namespace TestNopApi.Models
{
    public class PractitionerReferralDto
    {
        public int Id { get; internal set; }

        public string Code { get; internal set; }

        [JsonPropertyName("last-name")]
        public string LastName { get; internal set; }

        public int Discount { get; internal set; }

        [JsonPropertyName("dispensary-link")]
        public string DispensaryLink { get; set; }
    }
}
